import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { injectGlobal } from 'reakit';
import { Provider as ThemeProvider } from 'reakit';
import theme from './theme';

import {
  Capsules,
  Company,
  Cores,
  Dragons,
  History,
  Home,
  Launches,
  LaunchPads,
  Missions,
  NotFound,
  Payloads,
  Rockets,
  Roadster,
  Ships
} from './pages';

injectGlobal`
  @import url('https://fonts.googleapis.com/css?family=Anonymous+Pro|Dosis|Slabo+27px');
  html {
    font-size: 16px;
  }
  body {
    margin: 0;
    font-family: 'Slabo 27px', sans-serif;
  }
`;

const App = () => (
  <ThemeProvider theme={theme}>
    <Router>
      <Switch>
        <Route path="/capsules" component={Capsules}/>
        <Route path="/company" component={Company}/>
        <Route path="/cores" component={Cores}/>
        <Route path="/dragons" component={Dragons}/>
        <Route path="/history" component={History}/>
        <Route path="/launches" component={Launches}/>
        <Route path="/launch-pads" component={LaunchPads}/>
        <Route path="/missions" component={Missions}/>
        <Route path="/payloads" component={Payloads}/>
        <Route path="/rockets" component={Rockets}/>
        <Route path="/roadster" component={Roadster}/>
        <Route path="/ships" component={Ships}/>
        <Route exact path="/" component={Home}/>
        <Route component={NotFound} />
      </Switch>
    </Router>
  </ThemeProvider>
);

export default App;
