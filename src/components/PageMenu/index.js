import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import { Block, Heading, Link, List } from 'reakit';

const LinkSidebar = () => (
  <Block>
    <Heading as="h2">Browse by:</Heading>
    <List>
      <li><Link as={RouterLink} to={'/capsules'}>Capsules</Link></li>
      <li><Link as={RouterLink} to={'/company'}>Company</Link></li>
      <li><Link as={RouterLink} to={'/cores'}>Cores</Link></li>
      <li><Link as={RouterLink} to={'/dragons'}>Dragons</Link></li>
      <li><Link as={RouterLink} to={'/history'}>History</Link></li>
      <li><Link as={RouterLink} to={'/launches'}>Launches</Link></li>
      <li><Link as={RouterLink} to={'/launch-pads'}>Launch Pads</Link></li>
      <li><Link as={RouterLink} to={'/missions'}>Missions</Link></li>
      <li><Link as={RouterLink} to={'/payloads'}>Payloads</Link></li>
      <li><Link as={RouterLink} to={'/rockets'}>Rockets</Link></li>
      <li><Link as={RouterLink} to={'/roadster'}>Roadster</Link></li>
      <li><Link as={RouterLink} to={'/ships'}>Ships</Link></li>
    </List>
  </Block>
);

export default LinkSidebar;
