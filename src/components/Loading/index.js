import React from 'react';
import { styled, keyframes, Block } from 'reakit';

const pulse = keyframes`
  0% { box-shadow: 0 0 0 0 rgba(0, 82, 136, 0.4); }
  70% { box-shadow: 0 0 0 10px rgba(204, 169, 44, 0); }
  100% { box-shadow: 0 0 0 0 rgba(204, 169, 44, 0); }
`;

const Pulse = styled(Block)`
  margin: 0 auto;
  display: block;
  width: 22px;
  height: 22px;
  border-radius: 50%;
  background: #005288;
  cursor: pointer;
  box-shadow: 0 0 0 rgba(0, 82, 136, 0.4);
  animation: ${pulse} 2s infinite;
`;

export default () => (
  <div>
    <Pulse />
    <p>Loading&hellip;</p>
  </div>
);
