import React from 'react';
import {styled, Flex } from 'reakit';
import Loading from '../Loading';
import DataViewer from '../DataViewer';

const CenterLoading = styled(Flex)`
  align-items: center;
  justify-content: center;
  height: 50vh;
  width: 100%;
`;

class ApiQuery extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = { data: null };
  }

  componentDidMount() {
    this.props.endpoint().then(
      data => this.setState({ data: data.data })
    );
  }

  render() {
    const { data } = this.state;
    return !!data
      ? <DataViewer data={data} />
      : <CenterLoading><Loading /></CenterLoading>;
  }
}

export default ApiQuery;
