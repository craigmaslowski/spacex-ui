import React from 'react';
import { Link } from 'react-router-dom';
import PageMenu from '../PageMenu';
import Logo from '../Logo';
import Page from '../Page';

const DefaultPage = ({children}) => (
  <Page>
    <Page.Header>
      <Link to="/"><Logo /></Link>
    </Page.Header>
    <Page.Sidebar>
      <PageMenu />
    </Page.Sidebar>
    <Page.Content>
      {children}
    </Page.Content>
  </Page>
);

export default DefaultPage;
