import { styled } from 'reakit';

const Page = styled.div`
  display: grid;
  grid-template-rows: auto;
  grid-template-columns: 75% 25%;
  grid-template-areas:
    "header header"
    "content sidebar"
`;

Page.Header = styled.div`
  grid-area: header;
  padding: 20px;
  width: 100%;
`;

Page.Content = styled.div`
  grid-area: content;
  padding: 0 40px;
`;

Page.Sidebar = styled.div`
  grid-area: sidebar;
  padding: 0 40px;
`;

export default Page;
