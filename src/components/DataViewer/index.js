import React from 'react';
import { styled } from 'reakit';

const CodeBlock = styled.pre`
  background: #d9dce0;
  border: solid 1px #c0c3c6;
  border-radius: 4px;
  font-family: 'Anonymous Pro', monospace;
  height: 50vh;
  max-height: 50vh;
  overflow-y: scroll;
  padding: 20px;
`;

const DataViewer = ({data}) => (
  <CodeBlock>{JSON.stringify(data, null, 2)}</CodeBlock>
);

export default DataViewer;
