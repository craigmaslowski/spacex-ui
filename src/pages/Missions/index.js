import React from 'react';
import spacex from '../../services/spacex';
import { Heading } from 'reakit';
import { ApiQuery, DefaultPage } from '../../components';

const Missions = () => (
  <DefaultPage>
    <Heading>Missions</Heading>
    <ApiQuery endpoint={spacex.missions.getAll} />
  </DefaultPage>
);

export default Missions;
