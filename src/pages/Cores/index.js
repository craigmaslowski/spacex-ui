import React from 'react';
import spacex from '../../services/spacex';
import { Heading } from 'reakit';
import { ApiQuery, DefaultPage } from '../../components';

const Cores = () => (
  <DefaultPage>
    <Heading>Cores</Heading>
    <ApiQuery endpoint={spacex.cores.getAll} />
  </DefaultPage>
);

export default Cores;
