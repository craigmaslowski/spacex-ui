import React from 'react';
import spacex from '../../services/spacex';
import { Heading } from 'reakit';
import { ApiQuery, DefaultPage } from '../../components';

const Payloads = () => (
  <DefaultPage>
    <Heading>Payloads</Heading>
    <ApiQuery endpoint={spacex.payloads.getAll} />
  </DefaultPage>
);

export default Payloads;
