import React from 'react';
import spacex from '../../services/spacex';
import { Heading } from 'reakit';
import { ApiQuery, DefaultPage } from '../../components';

const Ships = () => (
  <DefaultPage>
    <Heading>Ships</Heading>
    <ApiQuery endpoint={spacex.ships.getAll} />
  </DefaultPage>
);

export default Ships;
