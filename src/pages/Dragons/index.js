import React from 'react';
import spacex from '../../services/spacex';
import { Heading } from 'reakit';
import { ApiQuery, DefaultPage } from '../../components';

const Dragons = () => (
  <DefaultPage>
    <Heading>Dragons</Heading>
    <ApiQuery endpoint={spacex.dragons.getAll} />
  </DefaultPage>
);

export default Dragons;
