import React from 'react';
import spacex from '../../services/spacex';
import { Heading } from 'reakit';
import { ApiQuery, DefaultPage } from '../../components';

const Launches = () => (
  <DefaultPage>
    <Heading>Launches</Heading>
    <ApiQuery endpoint={spacex.launches.getAll} />
  </DefaultPage>
);

export default Launches;
