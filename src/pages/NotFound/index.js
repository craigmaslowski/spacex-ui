import React from 'react';
import { Heading, Image } from 'reakit';
import { DefaultPage } from '../../components';

const Capsules = () => (
  <DefaultPage>
    <Heading>Lost In Space&hellip;</Heading>
    <p>Sorry, we couldn't find that page.</p>
    <Image src="/images/spacex-explosion.jpg" width="50vw" />
  </DefaultPage>
);

export default Capsules;
