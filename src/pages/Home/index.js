import React from 'react';
import spacex from '../../services/spacex';
import { Heading } from 'reakit';
import { ApiQuery, DefaultPage } from '../../components';

const Capsules = () => (
  <DefaultPage>
    <Heading>SpaceX Data Browser</Heading>
    <ApiQuery endpoint={spacex.company.get} />
  </DefaultPage>
);

export default Capsules;
