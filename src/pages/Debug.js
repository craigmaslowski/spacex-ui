const capsule_mock = {
  "capsule_serial": "C103",
  "capsule_id": "dragon1",
  "status": "unknown",
  "original_launch": "2012-10-08T00:35:00.000Z",
  "original_launch_unix": 1349656500,
  "missions": [
      {
          "name": "CRS-1",
          "flight": 9
      }
  ],
  "landings": 1,
  "type": "Dragon 1.0",
  "details": "First of twenty missions flown under the CRS1 contract",
  "reuse_count": 0
};
