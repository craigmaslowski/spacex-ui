import React from 'react';
import spacex from '../../services/spacex';
import { Heading } from 'reakit';
import { ApiQuery, DefaultPage } from '../../components';

const Rockets = () => (
  <DefaultPage>
    <Heading>Rockets</Heading>
    <ApiQuery endpoint={spacex.rockets.getAll} />
  </DefaultPage>
);

export default Rockets;
