import React from 'react';
import spacex from '../../services/spacex';
import { Heading } from 'reakit';
import { ApiQuery, DefaultPage } from '../../components';

const Roadster = () => (
  <DefaultPage>
    <Heading>Roadster</Heading>
    <ApiQuery endpoint={spacex.roadster.get} />
  </DefaultPage>
);

export default Roadster;
