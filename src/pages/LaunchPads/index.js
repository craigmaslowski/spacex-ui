import React from 'react';
import spacex from '../../services/spacex';
import { Heading } from 'reakit';
import { ApiQuery, DefaultPage } from '../../components';

const LaunchPads = () => (
  <DefaultPage>
    <Heading>Launch Pads</Heading>
    <ApiQuery endpoint={spacex.launchpads.getAll} />
  </DefaultPage>
);

export default LaunchPads;
