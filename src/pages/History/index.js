import React from 'react';
import spacex from '../../services/spacex';
import { Heading } from 'reakit';
import { ApiQuery, DefaultPage } from '../../components';

const History = () => (
  <DefaultPage>
    <Heading>History</Heading>
    <ApiQuery endpoint={spacex.history.getAll} />
  </DefaultPage>
);

export default History;
