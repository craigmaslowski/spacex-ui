import React from 'react';
import spacex from '../../services/spacex';
import { Heading } from 'reakit';
import { ApiQuery, DefaultPage } from '../../components';

const Capsules = () => (
  <DefaultPage>
    <Heading>Capsules</Heading>
    <ApiQuery endpoint={spacex.capsules.getAll} />
  </DefaultPage>
);

export default Capsules;
