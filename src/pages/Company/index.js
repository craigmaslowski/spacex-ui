import React from 'react';
import spacex from '../../services/spacex';
import { Heading } from 'reakit';
import { ApiQuery, DefaultPage } from '../../components';

const Company = () => (
  <DefaultPage>
    <Heading>Company</Heading>
    <ApiQuery endpoint={spacex.company.get} />
  </DefaultPage>
);

export default Company;
