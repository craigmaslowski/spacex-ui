import React from 'react';
import { connect } from 'react-redux';
import { getTileData } from '../selectors';

const withTile = Component => {
  const WithTile = props => <Component {...props} />;
  return connect(getTileData)(WithTile);
};

export default withTile;
