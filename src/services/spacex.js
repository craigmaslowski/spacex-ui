import axios from 'axios';

const SPACEX_API_ROOT = 'https://api.spacexdata.com/v3';
const endpoints = {
  capsules: `${SPACEX_API_ROOT}/capsules`,
  cores: `${SPACEX_API_ROOT}/cores`,
  dragons: `${SPACEX_API_ROOT}/dragons`,
  history: `${SPACEX_API_ROOT}/history`,
  info: `${SPACEX_API_ROOT}/info`,
  launches: `${SPACEX_API_ROOT}/launches`,
  launchpads: `${SPACEX_API_ROOT}/launchpads`,
  missions: `${SPACEX_API_ROOT}/missions`,
  payloads: `${SPACEX_API_ROOT}/payloads`,
  rockets: `${SPACEX_API_ROOT}/rockets`,
  roadster: `${SPACEX_API_ROOT}/roadster`,
  ships: `${SPACEX_API_ROOT}/ships`
};

const get = (resource, modifier) => {
  const endpoint = endpoints[resource];
  const url = modifier ? `${endpoint}/${modifier}` : endpoint;
  return axios.get(url);
};

const capsules = {
  getAll: () => get('capsules'),
  getOne: (id) => get('capsules', id)
};

const company = {
  get: () => get('info')
};

const cores = {
  getAll: () => get('cores'),
  getOne: (id) => get('cores', id)
};

const dragons = {
  getAll: () => get('dragons'),
  getOne: (id) => get('dragons', id)
};

const history = {
  getAll: () => get('history'),
  getOne: (id) => get('history', id)
};

const launches = {
  getAll: () => get('launches'),
  getLatest: () => get('launches', 'latest'),
  getNext: () => get('launches', 'next'),
  getOne: (id) => get('launches', id),
  getPast: () => get('launches', 'past'),
  getUpcoming: () => get('launches', 'upcoming')
};

const launchpads = {
  getAll: () => get('launchpads'),
  getOne: (id) => get('launchpads', id)
};

const missions = {
  getAll: () => get('missions'),
  getOne: (id) => get('missions', id)
};

const payloads = {
  getAll: () => get('payloads'),
  getOne: (id) => get('payloads', id)
};

const rockets = {
  getAll: () => get('rockets'),
  getOne: (id) => get('rockets', id)
};

const roadster = {
  get: () => get('roadster')
};

const ships = {
  getAll: () => get('ships'),
  getOne: (id) => get('ships', id)
};

export default {
  capsules,
  company,
  cores,
  dragons,
  history,
  launches,
  launchpads,
  missions,
  payloads,
  rockets,
  roadster,
  ships
};
